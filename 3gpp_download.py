from ftplib import FTP
import re
import sys,os
import threading
from Queue import Queue

def connectServer():
	ftp = None
	try:
		ftp = FTP('www.3gpp.org', 'anonymous', 'anonymous')
		ftp.cwd('Specs/archive/')
	except:
		s = sys.exc_info()
		print "Unexpected exception: %s: %s: on line %d" % (s[0], s[1], s[2].tb_lineno)

		if ftp:
			ftp.quit()
			ftp = None
	return ftp

class DownloadThread(threading.Thread):
	def __init__(self, queue):
		threading.Thread.__init__(self);
		self.ftp = None
		self.queue = queue
		self.quit = False

	def run(self):
		self.ftp = connectServer()
		if not self.ftp:
			return

		while not self.quit:
			filepath = self.queue.get(True)
			if filepath == -1: #-1 means that there is no more file need to be downlaoded in queue
				print 'No more work to do!!'
				break;

			if self.quit: #Check quit flag is set
				break

			if filepath == None: #Invalid filepath, skip
				continue

			local_filepath = filepath.replace('/', os.sep)
			if os.path.isfile(local_filepath): #File already exist on local
				sys.stdout.write('Skip %s\n'% filepath)
				continue

			local_path = os.path.dirname(local_filepath)
			if not os.path.exists(local_path):
				f = None
				try:
					os.makedirs(local_path)
					f = open(local_filepath,'wb')
					sys.stdout.write('Downloading %s ...' % filepath)
					self.ftp.retrbinary('RETR %s' % filepath, f.write)
					sys.stdout.write('[OK]\n')
				except:
					s = sys.exc_info()
					print "Unexpected exception: %s: %s: on line %d" % (s[0], s[1], s[2].tb_lineno)
					self.quit = True
				finally:
					if f:
						f.close()
						f = None

		if self.ftp:
			self.ftp.quit()
			self.ftp = None

	def forceQuit(self):
		self.quit = True
		if self.ftp:
			try:
				self.ftp.quit()
			except:
				s = sys.exc_info()
				print "Unexpected exception-samdebug: %s: %s: on line %d" % (s[0], s[1], s[2].tb_lineno)
			self.ftp = None

class DownloadManagerThread(threading.Thread):
	def __init__(self, queue, thread_list):
		threading.Thread.__init__(self);
		self.ftp = None
		self.queue = queue
		self.thread_list = thread_list

	def run(self):
		self.ftp = connectServer()
		if not self.ftp:
			return

		if not self.__fetching_file_list():
			for t in self.thread_list:
				t.forceQuit()

		while 1:
			self.queue.put(-1, True)

	def forceQuit(self):
		self.quit = True
		if self.ftp:
			try:
				self.ftp.quit()
			except:
				s = sys.exc_info()
				print "Unexpected exception: %s: %s: on line %d" % (s[0], s[1], s[2].tb_lineno)
			self.ftp = None

	def __fetching_file_list(self):
		p_spec_serials = re.compile('\./\d{2}_series') #Match directory of 3GPP spec
		p_spec_number = re.compile(r'\./(\d{2})_series/\1\.[\dabcdef]{2,3}') #Match directory of 3GPP spec number like: 23.054
		p_spec_file = re.compile('.*\.zip$')

		try:
			print self.ftp.getwelcome()

			list_1 = self.ftp.nlst('./') # Get spec serial list
			for line_1 in list_1:
				if p_spec_serials.match(line_1):
					list_2 = self.ftp.nlst('%s/' % line_1) # Get spec number list of specified spec serial
					for line_2 in list_2:
						if p_spec_number.match(line_2):
							list_3 = self.ftp.nlst('%s/' % line_2) # Get spec file list of specified spec number
							latest_file = None
							for line_3 in list_3[::-1]:
								if (p_spec_file.match(line_3.lower())):
									latest_file = line_3
									break;
							if latest_file:
								self.queue.put(latest_file, True)
		except:
			s = sys.exc_info()
			print "Unexpected exception: %s: %s: on line %d" % (s[0], s[1], s[2].tb_lineno)
			return False
		return True

if __name__ == '__main__' :
	TIME_INFINITE = 3600 * 24 * 366 * 100 # For me, 100 years equal infinite
	download_thread_num = 20
	thread_list = []
	fileq = Queue(20)

	for i in xrange(1, download_thread_num):
		t = DownloadThread(fileq)
		t.start()
		thread_list.append(t)

	mgrthread = DownloadManagerThread(fileq, thread_list)
	mgrthread.setDaemon(True)
	mgrthread.start()

	while 1:
		print 'wait thread exit - start'
		try:
			for t in thread_list:
				t.join(TIME_INFINITE)
		except:
			mgrthread.forceQuit()
			for t in thread_list:
				t.forceQuit()
		else:
			break
		print 'wait thread exit - end'

	print 'Main Exit!!'
